import React, { Component, Fragment } from 'react';
import './App.css';
import HelloState from './components/HelloState';
import HelloButton from './components/HelloButton';
import HelloAlert from './components/HelloAlert';
import HelloArray from './components/HelloArray';
import HelloJoin from './components/HelloJoin';
import HelloLower from './components/HelloLower';
import HelloUpper from './components/HelloUpper';
import HelloReverse from './components/HelloReverse';
import HelloJSONN from './components/HelloJSONN';

class App extends Component {
  render() {
    const date = Date(Date.now());
    const helloWorld = "Hello World";
    const concat = helloWorld.concat(" hello !")
    return (
      <Fragment className="fragmento">
        <HelloState date={date} />  {/*usando state */} 
        <HelloButton date={date} /> {/*usando botão */}
        <HelloAlert date={date} />  {/*usando alert */}
        <HelloArray date={date} />  {/*mapeando array */}
        <HelloLower date={date} helloWorld={helloWorld} />  {/*usando minusculo */}
        <HelloReverse date={date} helloWorld={helloWorld} />  {/*de trás pra frente */}
        <HelloUpper date={date} helloWorld={helloWorld} />  {/*usando maiusculo */}
        <HelloJoin date={date} helloWorld={helloWorld} /> {/*split com join */}
        <HelloJSONN date={date} />  {/*mostrando json */}
        <h3>{concat}</h3> {/*usando concat */}
      </Fragment>
    );
  };
};
export default App;
