import React from 'react';

const HelloUpper = (props) => {
    return (
        <h3>
            {props.helloWorld.toUpperCase()}
            <br></br>
            {props.date}
        </h3>
    );
};

export default HelloUpper;