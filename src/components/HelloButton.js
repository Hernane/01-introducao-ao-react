import React from 'react';

class HelloButton extends React.Component {
    state = {
        valor: "Click Me"
    }
    render() {
        const mudarState = () => this.setState({ valor: "Hello world" });
        return (
            <h3>
                <button onClick={mudarState}>{this.state.valor}</button>
                <br></br>
                {this.props.date}
            </h3>
        );
    };
};

export default HelloButton;