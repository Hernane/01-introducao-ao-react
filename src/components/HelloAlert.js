import React from 'react';

export default class HelloAlert extends React.Component {
    render() {
        return (
            <h3>
                <button onClick={() => alert("hello world")}>
                    Click me
                </button>
                <br></br>
                {this.props.date}
            </h3>
        )
    }
}