import React, { Component } from 'react';

class HelloState extends Component {
    state={
        hello1: "hello world"
    }
    render() {
        return (
            <h3>
                {this.state.hello1}
                <br></br>
                {this.props.date}
            </h3>
        );
    };
};

export default HelloState;