import React from 'react';

const HelloLower = (props) => {
    return (
        <h3>
            {props.helloWorld.toLowerCase()}
            <br></br>
            {props.date}
        </h3>
    );
};

export default HelloLower;