import React from 'react';
const { hello } = require('../components/HelloJSON.json')

const HelloJSONN = (props) => {
    const helloJson = hello[1];
    const helloString = JSON.stringify(helloJson);
    return (
        <h3>
            {helloString}
            <br></br>
            {props.date}
        </h3>
    );
};

export default HelloJSONN;