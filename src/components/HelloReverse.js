import React from 'react';

const HelloReverse = (props) => {
    return (
        <h3>
            {props.helloWorld.split('').reverse()}
            <br></br>
            {props.date}
        </h3>
    );
};

export default HelloReverse;