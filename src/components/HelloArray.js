import React from 'react';

const helloArray = [
    "hello 1 world",
    "hello 2 world",
    "hello 3 world"
];

const HelloArray = (props) => {
    const hello1 = helloArray.map((value, index) =>
        <div key={index} >
            {value}
        </div>
    );
    return (
        <h3>
            {hello1}
            <br></br>
            {props.date}
        </h3>
    );
};

export default HelloArray;