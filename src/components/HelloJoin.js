import React from 'react';

const HelloJoin = (props) => {
    return (
        <h3>
            {props.helloWorld.split('').join('^')}
            <br></br>
            {props.date}
        </h3>
    );
};

export default HelloJoin;